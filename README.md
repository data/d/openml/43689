# OpenML dataset: PC-Games-2020

https://www.openml.org/d/43689

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The projects goal is to use this dataset to predict the level of success game developers should expect given their game design details. Features such as 'Indie' (developed by indie studio), 'Soundtrack' (whether or not the game was noted for its soundtrack), and 'Genres', will be able to predict the popularity of the game.
Content
Gathered the data July 2020 by doing one long scrape of the Steam store, from most popular to least popular. You can see signs of this by the correlation between index and the presence value (number of online posts related to the game).
While performing the scrape, each game was supplemented by calling the RAWG API and adding another dozen or so features.
Inspiration
My main inspiration with this dataset was to gain and share the importance of each of the features related to game success on the Steam store. This information could be valuable for game developers, and I would also like to create a game using the insights, to evaluate the accuracy of the models.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43689) of an [OpenML dataset](https://www.openml.org/d/43689). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43689/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43689/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43689/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

